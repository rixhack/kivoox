import os
import sys
import urllib
import urlparse
import xbmcaddon
import xbmcgui
import xbmcplugin
import requests
from bs4 import BeautifulSoup

frontPageUrl = "https://www.ivoox.com/audios_sa_f_1.html"
#addon_handle = int(sys.argv[1])


def getAudioURL(id):
    return "http://ivoox.com/listen_mn_" + id + "_1.mp3"

def build_url(query):
    base_url = sys.argv[0]
    return base_url + '?' + urllib.urlencode(query)

def get_page(url):
    return BeautifulSoup(requests.get(url).text, 'html.parser')

def parse_page(page):
    podcasts = {}
    index = 1
    for item in page.find_all('a'):
        if item.has_attr('href'):    
            if item['href'].find('-mp3') > 1:
                cover = ''
                if (item.find('img') != None):
                    cover = item.find('img')['data-src'].split('url=')[1]
                    podcasts.update({index: {'title': item['title'].encode('utf-8'), 'url': getAudioURL(item['href'].split('rf_')[1].split('_1')[0].encode('utf-8')), 'cover': cover}})
                    index +=1
    return podcasts

def build_list(podcasts):
    podcast_list = []
    for podcast in podcasts:
        li = xbmcgui.ListItem(label=podcasts[podcast]['title'], thumbnailImage=podcasts[podcast]['cover'])
        li.setProperty('IsPlayable', 'true')
        url = build_url({'mode': 'stream', 'url': podcasts[podcast]['url'], 'title': podcasts[podcast]['title']})
        podcast_list.append((url, li, False))

    xbmcplugin.addDirectoryItems(addon_handle, podcast_list, len(podcast_list))
    xbmcplugin.setContent(addon_handle,'songs')
    xbmcplugin.endOfDirectory(addon_handle)

def play_podcast(url):
    play_item = xbmcgui.ListItem(path=url)
    xbmcplugin.setResolvedUrl(addon_handle, True, listitem=play_item)

def main():
    args = urlparse.parse_qs(sys.argv[2][1:])
    mode = args.get('mode', None)
    if mode is None:
        page = get_page(frontPageUrl)
        content = parse_page(page)
        build_list(content)
    elif mode[0] == 'stream':
        play_podcast(args['url'][0])

if __name__ == '__main__':
    addon_handle = int(sys.argv[1])
    main()
